$(document).ready(function() {
    // Slide down the login form after 2 seconds
    setTimeout(function(){
      $(".login-form").slideDown();
    }, 2000);
  
    // Change input border color to green when user types
    $("input").on("input", function() {
      $(this).css("border-color", "green");
    });
  
    // Add class "form-control-sm" to all input type elements
    $("input").addClass("form-control-sm");
  
    // Disable the login button and show loader when clicked
    $("#login-btn").on("click", function() {
      $(this).prop("disabled", true);
      $(this).html('<i class="fa fa-spinner fa-spin"></i> Loading...');
      // perform login logic here
    });
  });
  